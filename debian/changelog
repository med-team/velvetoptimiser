velvetoptimiser (2.2.6-6) UNRELEASED; urgency=medium

  * Remove Tim Booth <tbooth@ceh.ac.uk> since address is bouncing
    (Thank you for your work on this package, Tim)

 -- Andreas Tille <tille@debian.org>  Mon, 16 Sep 2024 12:52:12 +0200

velvetoptimiser (2.2.6-5) unstable; urgency=medium

  * Fix watch file
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 06 Dec 2022 16:23:24 +0100

velvetoptimiser (2.2.6-4) unstable; urgency=medium

  [ Steffen Moeller ]
  * Fix watchfile to detect new versions on github (routine-update)

  [ Andreas Tille ]
  * Standards-Version: 4.6.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Fri, 24 Sep 2021 11:54:25 +0200

velvetoptimiser (2.2.6-3) unstable; urgency=medium

  * Team Upload.
  [ Steffen Möller ]
  * Update metadata with ref to conda

  [ Nilesh Patra ]
  * Add autopkgtests
  * Fix shebang
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/rules (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Nilesh Patra <npatra974@gmail.com>  Fri, 09 Oct 2020 19:46:41 +0530

velvetoptimiser (2.2.6-2) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.4
  * Fix doc installation
    Closes: #903193

 -- Andreas Tille <tille@debian.org>  Mon, 09 Jul 2018 09:38:14 +0200

velvetoptimiser (2.2.6-1) unstable; urgency=medium

  * New upstream version
  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.0 (no changes needed)

 -- Andreas Tille <tille@debian.org>  Fri, 22 Sep 2017 09:21:07 +0200

velvetoptimiser (2.2.5-5) unstable; urgency=medium

  * Homepage vanished - point to Github instead

 -- Andreas Tille <tille@debian.org>  Tue, 17 Jan 2017 11:18:28 +0100

velvetoptimiser (2.2.5-3) unstable; urgency=medium

  * cme fix dpkg-control
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Sat, 17 Dec 2016 22:50:19 +0100

velvetoptimiser (2.2.5-2) unstable; urgency=medium

  * Initial upload to Debian (Closes: #753301)

 -- Andreas Tille <tille@debian.org>  Mon, 30 Jun 2014 11:54:13 +0200

velvetoptimiser (2.2.5-1biolinux0.1) trusty; urgency=low

  * Auto-Rebuild for 14.04
  * Add watch file

 -- Tim Booth <tbooth@ceh.ac.uk>  Wed, 07 May 2014 18:06:48 +0100

velvetoptimiser (2.2.5-0ubuntu1) precise; urgency=low

  * New package
  * Remove use of FindBin

 -- Tim Booth <tbooth@ceh.ac.uk>  Fri, 23 Aug 2013 11:28:09 +0100
